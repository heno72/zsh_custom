### pre-oh-my-zsh .zshrc START
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/pete/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# Prompts are defined in 10_prompt.zsh

### pre-oh-my-zsh .zshrc END
