# PROMPT="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ ) %{$fg[cyan]%}%c%{$reset_color%}"

# Our xenobox prompt
# PROMPT='%F{2}[%n%f@%F{5}%m%f %F{4}%B%~]%b%f %# '
PROMPT="%{$fg_bold[green]%}[%n@%{$fg_bold[magenta]%}%m %{$fg_bold[blue]%}%1~%{$fg_bold[green]%}] "
PROMPT+='$(git_prompt_info)'
PROMPT+="%(?:%{$fg_bold[green]%}%# :%{$fg_bold[red]%}%# )%{$reset_color%}"

# Right prompt
# RPROMPT+='[%{$fg_bold[yellow]%}%?%{$reset_color%}]'
RPROMPT="[%(?:%{$fg_bold[yellow]%}%?:%{$fg_bold[red]%}%?)%{$reset_color%}]"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[blue]%}git:(%{$fg[red]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%}) %{$fg[yellow]%}✗"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"
