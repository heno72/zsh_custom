### Configuring zsh prompts ###
## Considering the parent repo of this file, it is assumed that
## ohmyzsh is present and is installed

# For ohmyzsh's git_prompt_info()
set_omz_theme_git(){
  ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[blue]%}git:(%{$fg[red]%}"
  ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
  ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%}) %{$fg[yellow]%}✗"
  ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"
}

# Enable promptinit
autoload -Uz promptinit &&promptinit

# Define prompts
prompt_production_setup() {
  PROMPT='%F{2}[%n%f@%F{5}%m%f %F{4}%B%~]%b%f %# '
  RPROMPT='[%F{3}%?%f]'
}

prompt_xenobox_setup() {
  PROMPT="%{$fg_bold[green]%}[%n@%{$fg_bold[magenta]%}%m %{$fg_bold[blue]%}%1~%{$fg_bold[green]%}] "
  PROMPT+='$(git_prompt_info)'
  PROMPT+="%(?:%{$fg_bold[green]%}%# :%{$fg_bold[red]%}%# )%{$reset_color%}"

  RPROMPT="%(?::[%{$fg_bold[red]%}%?%{$reset_color%}])"

  set_omz_theme_git
}

prompt_xenobox2l_setup() {
  PROMPT="%{$fg_bold[green]%}[%n@%{$fg_bold[magenta]%}%m %{$fg_bold[blue]%}%~%{$fg_bold[green]%}] "
  PROMPT+='$(git_prompt_info)'
  PROMPT+=$'\n'
  PROMPT+="%(?:%{$fg_bold[green]%}➜  :%{$fg_bold[red]%}➜  )%{$reset_color%}"

  RPROMPT="%(?::[%{$fg_bold[red]%}%?%{$reset_color%}])"

  set_omz_theme_git
}

# Add defined prompts to prompt_themes

prompt_themes+=( production )
prompt_themes+=( xenobox )
prompt_themes+=( xenobox2l )

# Invoke prompt change
prompt xenobox

